
public class Ausgabeformatierung1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String ausgabe1 = "Das ist ein Beispielsatz.";
		String ausgabe2 = "Ein Beispielsatz ist das.";
		
		String ausgabe3 = "Das ist ein \"Beispielsatz\".";
		String ausgabe4 = "Ein Beispielsatz ist das.";
		
		//Aufgabe1
		
		System.out.print(ausgabe1);	//print zeigt nur die Ausgaben ohne Zeilenumbruch
		System.out.println(ausgabe2); //println erstellt einen Zeilenumbruch
		
		System.out.print(ausgabe3 + " \n");
		System.out.print(ausgabe4);
		System.out.print("\n");
		
		//Aufgabe 2
		System.out.print("\n");
		
		System.out.println("      *");
		System.out.println("     ***");
		System.out.println("    *****");
		System.out.println("   *******");
		System.out.println("  *********");
		System.out.println(" ***********");
		System.out.println("*************");
		System.out.println("     *** ");
		System.out.println("     ***");
		
		//Aufgabe 3
		System.out.print("\n");
		
		System.out.printf("%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.34);
			}

}
