import java.util.Scanner;

public class Benzinverbrauch {

	public static void main(String[] args) {
		
		double stand, standtanken, tanken, ergebnis; 
		Scanner sc = new Scanner(System.in);
		
		System.out.println("km-Stand bei letzten Tanken: ");
		stand = sc.nextDouble();
		
		System.out.println("km-Stand beim Tanken.......: ");
		standtanken = sc.nextDouble();
		
		System.out.println("Benzinverbrauch (Liter)....: ");
		tanken = sc.nextDouble();
		
		ergebnis = standtanken - stand;
		ergebnis = ergebnis / 100;
		ergebnis = tanken / ergebnis;
		
		System.out.println("Der PKW hat " + ergebnis + " Liter auf 100 km verbraucht.");
	}

}
