
public class Temperaturtabelle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.printf("%-12s%-5s%10s\n", "Fahrenheit","|", "Celsius");
		System.out.println("----------------------------");
		System.out.printf("%-12s%-5s", "-20","|");
		System.out.printf(" %10.2f\n", -28.8889);
		System.out.printf("%-12s%-5s", "-10","|");
		System.out.printf(" %10.2f\n", -23.3333);
		System.out.printf("%-12s%-5s", "+0","|");
		System.out.printf(" %10.2f\n", -17.7778);
		System.out.printf("%-12s%-5s", "+20","|");
		System.out.printf(" %10.2f\n", -6.6667);
		System.out.printf("%-12s%-5s", "+30","|");
		System.out.printf(" %10.2f\n", -1.1111);
	}

}
